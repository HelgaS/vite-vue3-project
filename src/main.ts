import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './App.vue';
import HomePage from './pages/HomePage.vue';
import FilmPage from './pages/FilmPage.vue';
import NotFoundPage from './pages/NotFoundPage.vue';
import ProfilePage from './pages/ProfilePage.vue';
import '@/styles/index.scss';

import 'vuetify/styles';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';


const router = createRouter({
    routes: [
      {
        path: '/',
        name: 'home',
        component: HomePage
      },
      {
        path: '/:id',
        name: 'film',
        component: FilmPage
      },
      {
        path: '/profile',
        name: 'profile',
        component: ProfilePage,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/:pathMatch(.*)*',
        name: '404',
        component: NotFoundPage
      }
    ],
    history: createWebHistory()
})

// Для упрощения укажем это в переменной
const isAuthenticated = false;

router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !isAuthenticated) {
    return {
      name: 'home'
    }
  }
});

const vuetify = createVuetify({
  components,
  directives,
});

const app = createApp(App);
app.use(router);
app.use(vuetify);
app.mount('#app');
